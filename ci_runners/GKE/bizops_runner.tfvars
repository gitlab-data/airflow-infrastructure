network_mode = "default"
subnetwork= "gke-bizops-runner-subnet-37859e17"
min_master_version="1.24.10-gke.1200"
cluster_ipv4_cidr_block="10.52.0.0/14"
services_ipv4_cidr_block="10.0.16.0/20"
default_pool_node_count=1