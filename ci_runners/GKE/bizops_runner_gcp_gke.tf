provider "google" {
    project = "gitlab-analysis"
    region  = "us-west1"
    zone    = "us-west1-a" 
}

terraform {
  backend "gcs" {
    bucket  = "gitlab-analysis-data-terraform-state"
    prefix  = "bizops_runner/state"
  }
}

variable "network_mode" {
    type = string
}
variable "subnetwork" {
    type = string 
}
variable "min_master_version" {
  type =  string
}
variable "cluster_ipv4_cidr_block" {
    type = string
}
variable "services_ipv4_cidr_block" {
    type = string  
}
variable "default_pool_node_count" {
    type = number
}

resource "google_container_cluster" "bizops-runner" {
    project = "gitlab-analysis"
    location = "us-west1-a"
    provider = google-beta
    name     = "bizops-runner"
    network = var.network_mode
    networking_mode = "VPC_NATIVE"
    ip_allocation_policy {
        cluster_ipv4_cidr_block = var.cluster_ipv4_cidr_block
        services_ipv4_cidr_block = var.services_ipv4_cidr_block
    }
    subnetwork=var.subnetwork
    min_master_version = var.min_master_version
    addons_config {
      horizontal_pod_autoscaling {
        disabled=false
      }
    }
    maintenance_policy {
        daily_maintenance_window {
            start_time = "07:00"
        }
    }
   release_channel {
        channel="UNSPECIFIED"
    }
    private_cluster_config {
        enable_private_endpoint = false
    }
}

resource "google_container_node_pool" "default-pool" {
    name        = "default-pool"
    location = "us-west1-a"
    cluster     = google_container_cluster.bizops-runner.name
    node_count = var.default_pool_node_count
    version = var.min_master_version
    node_config {
        machine_type    = "n1-standard-2"
        image_type = "cos_containerd"
        disk_type = "pd-standard"
        disk_size_gb = 100
        preemptible = false        
    }
    upgrade_settings {
      max_surge=1
      max_unavailable=0
    }
    management {
      auto_repair=true
    }
}
