terraform {
  required_version = ">= 1.3"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 5.11.0"
    }

    vault = {
      source  = "hashicorp/vault"
      version = "~> 3.9"
    }
  }
}
