environment                             = "staging"
network_mode                            = "default"
subnetwork                              = "gke-bizops-runner-subnet-37859e17"
min_master_version                      = "1.25.10-gke.2700"
cluster_ipv4_cidr_block                 = "10.200.0.0/14"
services_ipv4_cidr_block                = "10.204.0.0/20"
highmem_pool_node_count                 = 1
testing_task_pool_node_count            = 1
extraction_task_pool_node_count         = 0
extraction_task_pool_highmem_node_count = 0
sales_analytics_pool_node_count         = 0
dbt_task_pool_node_count                = 1
google_cloud_sql_instance_name          = "airflow-data-ops-staging"