
### 

These manifests have been added for convenience, to provide the ability to create individual secrets when required. 
The terraform main.tf in the folder above will create all of these secrets and should be run when setting up a new cluster.   