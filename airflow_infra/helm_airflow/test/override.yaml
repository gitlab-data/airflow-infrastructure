# override.yaml
executor: LocalExecutor

webserverSecretKeySecretName: airflow-webserver-secret-key

fernetKeySecretName: airflow-fernet-key

postgresql:
  enabled: false

data:
  metadataSecretName: airflow-db-conn-string

createUserJob:
  useHelmHooks: false
  applyCustomEnv: false

migrateDatabaseJob:
  enabled: false
  useHelmHooks: false
  applyCustomEnv: false

config:
  webserver:
    warn_deployment_exposure: False

webserver:
  defaultUser:
    enabled: false

  labels:
    app: "airflow"
    run: "airflow"

  service:
    type: LoadBalancer
    ports:
    - name: airflow-ui
      port: 443
      targetPort: 8080

  waitForMigrations:
    enabled: false
  ## sidecar containers for web/scheduler/worker
  extraContainers:
    - name: cloud-sql-proxy
      ports:
        - containerPort: 5432
      image: gcr.io/cloudsql-docker/gce-proxy:1.33.3
      command: [ "/cloud_sql_proxy" ]
      args: [ "-instances", "gitlab-analysis:us-west1:airflow-staging-1-15-test-only=tcp:5432", "-credential_file", "/secrets/cloudsql-credentials"]
      securityContext:
        # The default Cloud SQL proxy image runs as the
        # "nonroot" user and group (uid: 65532) by default.
        runAsNonRoot: true
      volumeMounts:
        - mountPath: /secrets/
          name: gcp-airflow-service-account-volume
          readOnly: true

  extraVolumes:
    - name: gcp-airflow-service-account-volume
      secret:
        secretName: airflow


ingress:
  enabled: true
  ## WARNING: set as "networking.k8s.io/v1beta1" for Kubernetes 1.18 and earlier
  apiVersion: networking.k8s.io/v1
  web:
    annotations: {
      nginx.ingress.kubernetes.io/force-ssl-redirect: "true",
      nginx.ingress.kubernetes.io/listen-ports: '[{"HTTP": 80}, {"HTTPS":443}]',
    }
    hosts: [airflow.gitlabdata.com]
    path: "/"
    pathType: Prefix
    ingressClassName: "nginx"
    precedingPaths:
      - path: "/"
        pathType: Prefix
        serviceName: airflow-webserver
        servicePort: use-annotation
    tls:
      enabled: true
      secretName: airflow-tls
#
scheduler:
  labels:
    app: "airflow"
    run: "airflow"
  waitForMigrations:
    enabled: false
  ## sidecar containers for web/scheduler/worker
  extraContainers:
    - name: cloud-sql-proxy
      ports:
        - containerPort: 5432
      image: gcr.io/cloudsql-docker/gce-proxy:1.33.3
      command: [ "/cloud_sql_proxy" ]
      args: [ "-instances", "gitlab-analysis:us-west1:airflow-staging-1-15-test-only=tcp:5432", "-credential_file", "/secrets/cloudsql-credentials" ]
      securityContext:
        # The default Cloud SQL proxy image runs as the
        # "nonroot" user and group (uid: 65532) by default.
        runAsNonRoot: true
      volumeMounts:
        - mountPath: /secrets/
          name: gcp-airflow-service-account-volume
          readOnly: true

  extraVolumes:
    - name: gcp-airflow-service-account-volume
      secret:
        secretName: airflow

workers:
  ## sidecar containers for web/scheduler/worker
  extraContainers:
    - name: cloud-sql-proxy
      ports:
        - containerPort: 5432
      image: gcr.io/cloudsql-docker/gce-proxy:1.33.3
      command: [ "/cloud_sql_proxy" ]
      args: [ "-instances", "gitlab-analysis:us-west1:airflow-staging-1-15-test-only=tcp:5432", "-credential_file", "/secrets/cloudsql-credentials" ]
      securityContext:
        # The default Cloud SQL proxy image runs as the
        # "nonroot" user and group (uid: 65532) by default.
        runAsNonRoot: true
      volumeMounts:
        - mountPath: /secrets/
          name: gcp-airflow-service-account-volume
          readOnly: true

  extraVolumes:
    - name: gcp-airflow-service-account-volume
      secret:
        secretName: airflow

triggerer:
  enabled: false

statsd:
  enabled: false

dagProcessor:
  waitForMigrations:
    enabled: false

pgbouncer:
  enabled: true
  # The maximum number of connections to PgBouncer
  maxClientConn: 100
  # The maximum number of server connections to the metadata database from PgBouncer
  metadataPoolSize: 10
  # The maximum number of server connections to the result backend database from PgBouncer
  resultBackendPoolSize: 5

secret:
 - envName: "NAMESPACE"
   secretName: "airflow"
   secretKey: "NAMESPACE"

env:
  - name: "IN_CLOUD"
    value: "True"
  - name: "IN_CLUSTER"
    value: "False"
  - name: "GIT_BRANCH"
    value: "master"
  - name: "GOOGLE_APPLICATION_CREDENTIALS"
    value: "/secrets/cloudsql/cloudsql-credentials"
  - name: "AIRFLOW__CORE__MAX_ACTIVE_RUNS_PER_DAG"
    value: "1"