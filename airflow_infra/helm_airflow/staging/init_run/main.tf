provider "kubernetes" {
  config_path = "~/.kube/config"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

terraform {
  backend "gcs" {
    bucket  = "gitlab-analysis-data-terraform-state"
    prefix  = "airflow-prod/init-state"
  }
}

resource "kubernetes_persistent_volume_claim" "persistent_airflow_logs" {
  metadata {
    name      = "airflow-logs-pvc"
    namespace = "airflow"
  }

  spec {
    access_modes = ["ReadWriteMany"]
    storage_class_name = "standard-rwx"
    resources {
      requests = {
        storage = "600Gi"
      }
    }
  }
}

resource "helm_release" "airflow" {
  name       = "airflow"
  chart      = "apache-airflow/airflow"
  namespace = "airflow"
  # Helm chart version 1.9.0 = Airflow 2.5.3;
  # When upgrading can confirm by doing helm search repo airflow --versions
  version    = "1.9.0"

  values = [
    "${file("override.yaml")}"
  ]
  set {
    name  = "dags.persistence.enabled"
    value = "true"
  }
  set {
    name  = "dags.gitSync.enabled"
    value = "true"
  }
  set {
    name  = "dags.gitSync.repo"
    value = "https://gitlab.com/gitlab-data/analytics.git"
  }
  set {
    name  = "dags.gitSync.branch"
    value = "master"
  }
  set {
    name  = "dags.gitSync.subPath"
    value = "dags"
  }
  set {
    name  = "logs.persistence.enabled"
    value = "true"
  }
  set {
    name  = "logs.persistence.existingClaim"
    value = "airflow-logs-pvc"
  }

}

