provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_namespace" "airflow" {
  metadata {
    name = "airflow"
  }
}

resource "kubernetes_namespace" "testing" {
  metadata {
    name = "testing"
  }
}

resource "kubernetes_namespace" "staging" {
  metadata {
    name = "staging"
  }
}

resource "kubernetes_secret" "airflow" {
  metadata {
    name      = "airflow"
    namespace = "airflow"
  }
}

resource "kubernetes_secret" "airflow_testing" {
  metadata {
    name      = "airflow"
    namespace = "testing"
  }
}

resource "kubernetes_secret" "airflow_webserver_secret_key" {
  metadata {
    name      = "airflow-webserver-secret-key"
    namespace = "airflow"
  }
}

resource "kubernetes_secret" "airflow_db_conn_string" {
  metadata {
    name      = "airflow-db-conn-string"
    namespace = "airflow"
  }
}

resource "kubernetes_secret" "airflow_tls" {
  metadata {
    name      = "airflow-tls"
    namespace = "airflow"
  }
}