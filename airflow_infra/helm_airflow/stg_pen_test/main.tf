provider "kubernetes" {
  config_path = "~/.kube/config"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}


resource "helm_release" "airflow" {
  name       = "airflow"
  chart      = "apache-airflow/airflow"
  namespace = "airflow"
  # Helm chart version 1.9.0 = Airflow 2.5.3;
  # When upgrading can confirm by doing helm search repo airflow --versions
  version    = "1.9.0"

  values = [
    "${file("override.yaml")}"
  ]
  set {
    name  = "dags.persistence.enabled"
    value = "true"
  }
  set {
    name  = "dags.gitSync.enabled"
    value = "true"
  }
  set {
    name  = "dags.gitSync.repo"
    value = "https://gitlab.com/gitlab-data/analytics.git"
  }
  set {
    name  = "dags.gitSync.branch"
    value = "airflow_pen_test"
  }
  set {
    name  = "dags.gitSync.subPath"
    value = "dags"
  }
  set {
    name  = "logs.persistence.enabled"
    value = "true"
  }
  set {
    name  = "logs.persistence.existingClaim"
    value = "airflow-logs-pvc-pen-test"
  }

}

