provider "kubernetes" {
  config_path = "~/.kube/config"
}

terraform {
  backend "gcs" {
    bucket  = "gitlab-analysis-data-terraform-state"
    prefix  = "airflow-staging/init-state"
  }
}

resource "kubernetes_namespace" "testing" {
  metadata {
    name = "testing"
  }
}

resource "kubernetes_namespace" "staging" {
  metadata {
    name = "staging"
  }
}

resource "kubernetes_secret" "default" {
  metadata {
    name      = "airflow"
    namespace = "default"
  }
}

resource "kubernetes_secret" "testing" {
  metadata {
    name      = "airflow"
    namespace = "testing"
  }
}