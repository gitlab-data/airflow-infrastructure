provider "kubernetes" {
  config_path = "~/.kube/config"
}

terraform {
  backend "gcs" {
    bucket  = "gitlab-analysis-data-terraform-state"
    prefix  = "airflow-staging/state"
  }
}

variable "environment" {
    type = string
}

resource "kubernetes_namespace" "testing" {
  metadata {
    name = "testing"
  }
}

resource "kubernetes_namespace" "staging" {
  metadata {
    name = "staging"
  }
}

resource "kubernetes_persistent_volume_claim" "persistent_airflow_logs" {
  metadata {
    name      = "persistent-airflow-logs"
    namespace = "default"
  }

  spec {
    access_modes = ["ReadWriteOnce"]

    resources {
      requests = {
        storage = "10Gi"
      }
    }
  }
}

resource "kubernetes_ingress_v1" "airflow_ingress_staging" {
  metadata {
    name      = "airflow-ingress-${var.environment}"
    namespace = "default"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
      "nginx.ingress.kubernetes.io/force-ssl-redirect" = "true"
    }
  }

  spec {
    tls {
      hosts       = ["airflow.gitlabdata.com"]
      secret_name = "airflow-tls"
    }

    rule {
      host = "airflow.gitlabdata.com"

      http {
        path {
          path      = "/"
          path_type = "Prefix"

          backend {
            service {
              name = "airflow-webserver"

              port {
                number = 8080
              }
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "airflow_webserver_staging" {
  metadata {
    name      = "airflow-webserver-${var.environment}"
    namespace = "default"
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 8080
      target_port = "8080"
    }

    selector = {
      app = "airflow"
    }

    type = "NodePort"
  }

}


resource "kubernetes_deployment" "airflow_deployment_staging" {
  metadata {
    name      = "airflow-deployment-${var.environment}"
    namespace = "default"
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "airflow"
        run = "airflow"
      }
    }

    template {
      metadata {
        labels = {
          app = "airflow"
          run = "airflow"
        }
      }

      spec {
        volume {
          name = "airflow-secrets"
          secret {
            secret_name = "airflow"
          }
        }

        volume {
          name      = "analytics-repo"
        }

        volume {
          name = "airflow-logs"

          persistent_volume_claim {
            claim_name = "persistent-airflow-logs"
          }
        }

        volume {
          name      = "kube-config"
        }

        init_container {
          name    = "init-creds"
          image   = "registry.gitlab.com/gitlab-data/airflow-infrastructure/airflow-image:v0.0.2"
          command = ["/bin/sh", "-c"]
          args    = ["gcloud auth activate-service-account --key-file /secrets/cloudsql/cloudsql-credentials && gcloud container clusters get-credentials data-ops-staging --region us-west1-a --project gitlab-analysis"]

          volume_mount {
            name       = "airflow-secrets"
            read_only  = true
            mount_path = "/secrets/cloudsql/"
          }

          volume_mount {
            name       = "kube-config"
            mount_path = "/root/.kube/"
          }

          image_pull_policy = "Always"
        }

        init_container {
          name    = "init-repo"
          image   = "registry.gitlab.com/gitlab-data/airflow-infrastructure/airflow-image:v0.0.2"
          command = ["/bin/sh", "-c"]
          args    = ["git clone -b master --single-branch $REPO --depth 1"]

          env {
            name  = "REPO"
            value = "https://gitlab.com/gitlab-data/analytics.git"
          }

          volume_mount {
            name       = "analytics-repo"
            mount_path = "/usr/local/airflow/analytics"
          }

          image_pull_policy = "Always"
        }

        container {
          name    = "scheduler"
          image   = "registry.gitlab.com/gitlab-data/airflow-infrastructure/airflow-image:v0.0.2"
          command = ["airflow"]
          args    = ["scheduler"]

          env {
            name  = "GIT_BRANCH"
            value = "master"
          }

          env {
            name  = "IN_CLUSTER"
            value = "False"
          }

          env {
            name  = "GOOGLE_APPLICATION_CREDENTIALS"
            value = "/secrets/cloudsql/cloudsql-credentials"
          }

          env {
            name = "NAMESPACE"
            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "NAMESPACE"
              }
            }
          }

          env {
            name = "SLACK_API_TOKEN"
            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "SLACK_API_TOKEN"
              }
            }
          }

          env {
            name = "AIRFLOW__CORE__SQL_ALCHEMY_CONN"
            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "AIRFLOW__CORE__SQL_ALCHEMY_CONN"
              }
            }
          }

          env {
            name = "AIRFLOW__CORE__FERNET_KEY"

            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "AIRFLOW__CORE__FERNET_KEY"
              }
            }
          }

          env {
            name = "AIRFLOW__WEBSERVER__SECRET_KEY"

            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "AIRFLOW__WEBSERVER__SECRET_KEY"
              }
            }
          }

          resources {
            limits = {
              cpu = "4"
              memory = "4000Mi"
            }

            requests = {
              cpu = "1"
              memory = "1000Mi"
            }
          }

          volume_mount {
            name       = "airflow-secrets"
            read_only  = true
            mount_path = "/secrets/cloudsql/"
          }

          volume_mount {
            name       = "analytics-repo"
            mount_path = "/usr/local/airflow/analytics"
          }

          volume_mount {
            name       = "airflow-logs"
            mount_path = "/usr/local/airflow/logs"
          }

          volume_mount {
            name       = "kube-config"
            mount_path = "/root/.kube/"
          }

          lifecycle {
            post_start {
              exec {
                command = ["/bin/sh", "-c", "gcloud auth activate-service-account --key-file /secrets/cloudsql/cloudsql-credentials"]
              }
            }
          }

          image_pull_policy = "Always"
        }

        container {
          name    = "webserver"
          image   = "registry.gitlab.com/gitlab-data/airflow-infrastructure/airflow-image:v0.0.2"
          command = ["airflow"]
          args    = ["webserver"]

          port {
            container_port = 8080
            protocol       = "TCP"
          }

          env {
            name  = "GIT_BRANCH"
            value = "master"
          }

          env {
            name  = "IN_CLUSTER"
            value = "False"
          }

          env {
            name  = "GOOGLE_APPLICATION_CREDENTIALS"
            value = "/secrets/cloudsql/cloudsql-credentials"
          }

          env {
            name = "NAMESPACE"

            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "NAMESPACE"
              }
            }
          }

          env {
            name = "SLACK_API_TOKEN"

            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "SLACK_API_TOKEN"
              }
            }
          }

          env {
            name = "AIRFLOW__CORE__SQL_ALCHEMY_CONN"

            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "AIRFLOW__CORE__SQL_ALCHEMY_CONN"
              }
            }
          }

          env {
            name = "AIRFLOW__CORE__FERNET_KEY"

            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "AIRFLOW__CORE__FERNET_KEY"
              }
            }
          }

          env {
            name = "AIRFLOW__WEBSERVER__SECRET_KEY"

            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "AIRFLOW__WEBSERVER__SECRET_KEY"
              }
            }
          }

          resources {
            limits = {
              cpu = "2"
              memory = "2000Mi"
            }

            requests = {
              cpu = "400m"
              memory = "400Mi"
            }
          }

          volume_mount {
            name       = "airflow-secrets"
            read_only  = true
            mount_path = "/secrets/cloudsql/"
          }

          volume_mount {
            name       = "analytics-repo"
            mount_path = "/usr/local/airflow/analytics"
          }

          volume_mount {
            name       = "airflow-logs"
            mount_path = "/usr/local/airflow/logs"
          }

          volume_mount {
            name       = "kube-config"
            mount_path = "/root/.kube/"
          }

          liveness_probe {
            http_get {
              path   = "/health"
              port   = "8080"
              scheme = "HTTP"
            }

            initial_delay_seconds = 60
            timeout_seconds       = 10
            period_seconds        = 60
            success_threshold     = 1
            failure_threshold     = 3
          }

          readiness_probe {
            http_get {
              path   = "/health"
              port   = "8080"
              scheme = "HTTP"
            }

            initial_delay_seconds = 60
            timeout_seconds       = 10
            period_seconds        = 60
            success_threshold     = 1
            failure_threshold     = 3
          }

          image_pull_policy = "Always"
        }

        container {
          name    = "watcher"
          image   = "registry.gitlab.com/gitlab-data/airflow-infrastructure/airflow-image:v0.0.2"
          command = ["repo_watcher.py"]

          env {
            name = "AIRFLOW__CORE__SQL_ALCHEMY_CONN"

            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "AIRFLOW__CORE__SQL_ALCHEMY_CONN"
              }
            }
          }

          env {
            name = "AIRFLOW__CORE__FERNET_KEY"

            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "AIRFLOW__CORE__FERNET_KEY"
              }
            }
          }

          env {
            name = "AIRFLOW__WEBSERVER__SECRET_KEY"

            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "AIRFLOW__WEBSERVER__SECRET_KEY"
              }
            }
          }

          env {
            name  = "GIT_BRANCH"
            value = "master"
          }

          env {
            name  = "IN_CLUSTER"
            value = "False"
          }
           env {
            name = "NAMESPACE"

            value_from {
              secret_key_ref {
                name = "airflow"
                key  = "NAMESPACE"
              }
            }
          }

          resources {
            limits = {
              cpu = "100m"
              memory = "100Mi"
            }

            requests = {
              cpu = "50m"
              memory = "50Mi"
            }
          }

          volume_mount {
            name       = "analytics-repo"
            mount_path = "/usr/local/airflow/analytics"
          }

          volume_mount {
            name       = "airflow-secrets"
            read_only  = true
            mount_path = "/secrets/cloudsql/"
          }

          volume_mount {
            name       = "kube-config"
            mount_path = "/root/.kube/"
          }

          image_pull_policy = "Always"
        }

        container {
          name    = "cloudsql-proxy"
          image   = "gcr.io/cloudsql-docker/gce-proxy:1.33.3"
          command = ["/cloud_sql_proxy", "-instances=gitlab-analysis:us-west1:airflow-staging-1-15=tcp:5432", "-credential_file=/secrets/cloudsql/cloudsql-credentials"]

          resources {
            limits = {
              cpu = "1"
              memory = "1000Mi"
            }

            requests = {
              cpu = "500m"
              memory = "200Mi"
            }
          }

          volume_mount {
            name       = "airflow-secrets"
            read_only  = true
            mount_path = "/secrets/cloudsql/"
          }

          image_pull_policy = "Always"

          security_context {
            run_as_user = 2
          }
        }
      }
    }
  }
}
