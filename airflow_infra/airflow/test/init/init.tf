provider "kubernetes" {
  config_path = "~/.kube/config"
}

terraform {
  backend "gcs" {
    bucket  = "gitlab-analysis-data-terraform-state"
    prefix  = "airflow-test/init-state"
  }
}

resource "kubernetes_namespace" "testing" {
  metadata {
    name = "testing"
  }
}

resource "kubernetes_namespace" "staging" {
  metadata {
    name = "staging"
  }
}

resource "kubernetes_secret" "default" {
  metadata {
    name      = "airflow"
    namespace = "default"
  }
}

resource "kubernetes_secret" "testing" {
  metadata {
    name      = "airflow"
    namespace = "testing"
  }
}

resource "kubernetes_persistent_volume_claim" "persistent_airflow_logs" {
  metadata {
    name      = "persistent-airflow-logs"
    namespace = "default"
  }

  spec {
    access_modes = ["ReadWriteOnce"]
    storage_class_name = "standard"
    resources {
      requests = {
        storage = "10Gi"
      }
    }
  }
}